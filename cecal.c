//CeCal (See-Sill) is a simple, scriptable calculator optimized for understandability and performance. CeCal is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE.
//TODO:
//Add more comments to help normal people understand whats going on
//Make more efficient
//Add more functionality
//- logarithms
//- square roots
//- scientific notation (maybe)
//
//USAGE:
//command is cecal, can be followed by numbers and operators. Don't include whitespaces.
//example: ./cecal 1+6+9*5/8
//output: 10.00
//
//Download:
//git clone https://gitlab.com/ceasesolutions/cecal/
//gcc -o cecal cecal.c -lm
//
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>

double evaluate(const char* expr) {
    double result = 0.0;
    double temp_num = 0.0;
    char op = '+';
    int i = 0;

    while (expr[i] != '\0') {
        if (isdigit(expr[i])) {
            temp_num = (temp_num * 10) + (expr[i] - '0');
        }
        if (!isdigit(expr[i]) && !isspace(expr[i]) || expr[i + 1] == '\0') {
            if (op == '+') {
                result += temp_num;
            } else if (op == '-') {
                result -= temp_num;
            } else if (op == '*') {
                result *= temp_num;
            } else if (op == '/') {
                if (temp_num == 0) {
                    puts("Error: Division by zero\n");
                    exit(1);
                }
                result /= temp_num;
            } else if (op == '^') {
                result = pow(result, temp_num);
            } else if (op == 's') {
                result = sin(temp_num * M_PI / 180.0);
            } else if (op == 'c') {
                result = cos(temp_num * M_PI / 180.0);
            } else if (op == 't') {
                result = tan(temp_num * M_PI / 180.0);
            }
            op = expr[i];
            temp_num = 0.0;
        }
        i++;
    }

    return result;
}


int main(int argc, const char* argv[]) {
    if (argc != 2) {
        printf("Usage: calculator <expression>\n");
        return 1;
    }

    const char* expr = argv[1];
    double result = evaluate(expr);

    printf("%.2lf\n", result);
    return 0;
}
